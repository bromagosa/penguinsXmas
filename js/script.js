function init() {
	$(window).on('load', function(){
		var urlVars = getUrlVars();
		if(urlVars["user"]) { fillUp(urlVars) }
	});
}

function roll(className) {
	unroll(className);
	$(className).addClass('rolling');
}
function unroll(className) {
	$(className).removeClass('rolling');
}

function scrollTo(className) {
	$('body,html').animate({ scrollTop: $(className).offset().top }, $(className).offset().top * 0.7)
}

function clickedEnvelope(single,tune) {
	$("input.single").val(single);
	$("input.tune").val(tune);
	$('body,html').animate( 
		{scrollTop: 0} , 
		1000 , 
		function(){showEnvelope()}
	)
}

function showEnvelope(){
	$('div#site').css({opacity: 0.1});
	$('body').css({backgroundColor: "rgba(0,0,0,0.9)"});
	$('div#envelope').slideDown(1000 , function(){ $('form#card').fadeIn(500); });
}

function hideEnvelope(){
	$('div#envelope').fadeOut(500);
	$('form#card').hide();
	$('div#site').css({opacity: 1});
	$('body').css({backgroundColor: "transparent"});
}

function getUrlVars() {
	var vars = {};
	var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) { vars[key] = decodeURIComponent(value); });
	return vars;
}

function fillUp(urlVars) {
	$("div.happy2013.rpx").hide();
	$("span.sender-name").text(urlVars["user"] + " et desitja un");
	scrollTo("#logos");
	setTimeout( function() {
		scrollTo("div.vinyl." + urlVars["single"] + ".tune" + urlVars["tune"]);
		$("audio." + urlVars["single"] + ".tune" + urlVars["tune"])[0].play();
	} , 2000);
}
